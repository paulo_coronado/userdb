package com.example.userform;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserView> {

    private List<User> users;

    //Cuando creen el adaptador tienen pasar el listado de usuarios con el que va a trabajar
    public UserAdapter(List<User> users){
        this.users=users;
    }

    @NonNull
    @Override
    public UserView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //Colocar cada dato de la lista en un "cajón" desde la "plantila" userView
        Context ctx=parent.getContext();
        LayoutInflater creadorVistas= LayoutInflater.from(ctx);
        View cajonUsuario= creadorVistas.inflate(R.layout.userview,parent,false);
        UserView unCajon= new UserView(cajonUsuario);
        return unCajon;
    }

    @Override
    public void onBindViewHolder(@NonNull UserView holder, int position) {

        //Obtener un usuario desde la lista de usuarios
        User miUsuario= users.get(position);

        //Asociarlo a el textview para mostrarlo
        TextView dato=holder.nombre;
        dato.setText(miUsuario.nombre);
    }

    @Override
    public int getItemCount() {

        if(users==null){
            return 0;
        }
        return users.size();
    }
}
