package com.example.userform;


import android.content.Context;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {User.class}, version=1)

public abstract class UserDatabase extends RoomDatabase {

    private static volatile UserDatabase INSTANCE;

    public static UserDatabase getDatabase(Context ctx){
        //Conectarme a la db

        INSTANCE= androidx.room.Room.databaseBuilder(ctx.getApplicationContext(), UserDatabase.class,"usuarios").build();
        return  INSTANCE;
    }

    //Declarar los DAO que necesite
    public abstract  UserDAO userDao();
}
